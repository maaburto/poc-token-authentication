import AuthForm from "../../components/Auth/AuthForm";
import { useAuthAction } from "../hooks";

const AuthenticationWidget = () => {
  const { applyAuthenticity, isLoading, error } = useAuthAction();

  const loginHandler = async (values, isLogin) => {
    applyAuthenticity(values, isLogin);
  };

  return (
    <AuthForm
      onLogin={loginHandler}
      isLoading={isLoading}
      isErrorOnAuth={!!error}
    />
  );
};

export default AuthenticationWidget;
