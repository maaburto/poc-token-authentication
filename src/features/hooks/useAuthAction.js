import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../store/auth-context";

const FIREBASE_API_KEY = "AIzaSyB6Dwt0gaXuUV8XKTCS6giwne3sjqPwOvo";

const useAuthAction = () => {
  const authCtx = useContext(AuthContext);
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  useEffect(() => {
    if (error) {
      alert(error);
      setError(null);
    }
  }, [error]);

  const applyAuthenticity = async (values, isLogin) => {
    setIsLoading(true);
    let url = null;
    if (isLogin) {
      url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${FIREBASE_API_KEY}`;
    } else {
      url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${FIREBASE_API_KEY}`;
    }

    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify({
        email: values.email,
        password: values.password,
        returnSecureToken: true,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    setIsLoading(false);
    if (response.ok) {
      const data = await response.json();
      const expiresInBaseMillisecons = +data.expiresIn * 1000;
      const expirationTime = new Date(
        new Date().getTime() + expiresInBaseMillisecons
      );
      authCtx.login(data.idToken, expirationTime.toISOString());
      history.replace("/");
    } else {
      let errorMessage = "Authentication failed!";

      const data = await response.json();
      if (data && data.error && data.error.message) {
        errorMessage = data.error.message;
      }

      setError(errorMessage);
    }
  };

  return {
    isLoading,
    applyAuthenticity,
    error,
  };
};

export default useAuthAction;
