const TOKEN_KEYS = {
  TOKEN: "token",
  EXPIRATION_TIME: "expirationTime",
};

export const retrieveStoredToken = () => {
  const storedToken = localStorage.getItem(TOKEN_KEYS.TOKEN);
  const storedExpirationDate = localStorage.getItem(TOKEN_KEYS.EXPIRATION_TIME);
  const remainingTime = calculateRemainingTime(storedExpirationDate);

  const thresholdTimeToExpire = 60000; // 1 minute as treshold
  if (remainingTime <= thresholdTimeToExpire) {
    deleteStoredToken();

    return {
      token: null,
      duration: 0,
    };
  }

  return {
    token: storedToken,
    duration: remainingTime,
  };
};

export const deleteStoredToken = () => {
  localStorage.removeItem(TOKEN_KEYS.TOKEN);
  localStorage.removeItem(TOKEN_KEYS.EXPIRATION_TIME);
};

export const fillTokenToLocaStore = (token, expirationTime) => {
  localStorage.setItem(TOKEN_KEYS.TOKEN, token);
  localStorage.setItem(TOKEN_KEYS.EXPIRATION_TIME, expirationTime);
};

export const calculateRemainingTime = (expirationTime) => {
  const currentTime = new Date().getTime();
  const adjExpirationTime = new Date(expirationTime).getTime();

  return adjExpirationTime - currentTime;
};
