import React from "react";
import { useAuthTokens } from "./hooks";

export const AuthContext = React.createContext({
  isLoggedIn: false,
  token: null,
  login: (token) => {},
  logout: () => {},
});
AuthContext.displayName = "AuthContext";

export const AuthenticationProvider = (props) => {
  const { login, userIsLoggedIn, logout, token } = useAuthTokens();
  const contextValue = {
    token,
    isLoggedIn: userIsLoggedIn,
    login,
    logout,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthenticationProvider;
