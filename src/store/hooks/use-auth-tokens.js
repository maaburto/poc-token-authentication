import { useCallback, useEffect, useState } from "react";
import {
  calculateRemainingTime,
  deleteStoredToken,
  fillTokenToLocaStore,
  retrieveStoredToken,
} from "../services/token-storage";

let logoutTimer;

const useAuthTokens = () => {
  const tokenData = retrieveStoredToken();
  const [token, setToken] = useState(tokenData.token);
  const userIsLoggedIn = !!token;

  const logout = useCallback(() => {
    setToken(null);
    deleteStoredToken();

    if (logoutTimer) {
      clearTimeout(logoutTimer);
    }
  }, []);

  const login = (token, expirationTime) => {
    setToken(token);
    fillTokenToLocaStore(token, expirationTime);

    const remainingTime = calculateRemainingTime(expirationTime);
    logoutTimer = setTimeout(logout, remainingTime);
  };

  // Synchronizing (applying useEffect) Authentication statefull logic
  // from localstorage token & duration
  // useEffect(() => {
  //   const tokenData = retrieveStoredToken();
  //   setToken(tokenData.token);
  //   setDuration(tokenData.duration);
  //   console.log('tokenData', tokenData);
  // }, []);

  useEffect(() => {
    if (tokenData.duration) {
      logoutTimer = setTimeout(logout, tokenData.duration);
    }
  }, [tokenData.duration, logout]);

  return {
    userIsLoggedIn,
    login,
    logout,
    token,
  };
};

export default useAuthTokens;
