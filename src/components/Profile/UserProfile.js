import { useContext } from "react";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../store/auth-context";
import ProfileForm from "./ProfileForm";
import classes from "./UserProfile.module.css";

const FIREBASE_API_KEY = "AIzaSyB6Dwt0gaXuUV8XKTCS6giwne3sjqPwOvo";

const UserProfile = () => {
  const history = useHistory();
  const authCtx = useContext(AuthContext);
  const { token } = authCtx;

  const resetPasswordHanlder = (newPassword) => {
    fetch(
      `https://identitytoolkit.googleapis.com/v1/accounts:update?key=${FIREBASE_API_KEY}`,
      {
        method: "POST",
        body: JSON.stringify({
          idToken: token,
          password: newPassword,
          returnSecureToken: true,
        }),
        headers: {
          "Content-Type": "applicaton/json",
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        console.log("data", data);
        history.replace("/");
      });
  };

  return (
    <section className={classes.profile}>
      <h1>Your User Profile</h1>
      <ProfileForm onResetPassword={resetPasswordHanlder} />
    </section>
  );
};

export default UserProfile;
