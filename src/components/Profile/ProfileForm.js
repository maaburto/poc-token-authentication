import { useRef } from "react";
import classes from "./ProfileForm.module.css";

const ProfileForm = (props) => {
  const newPasswordInputRef = useRef();

  const submitHandler = (event) => {
    event.preventDefault();

    const enteredNewPassword = newPasswordInputRef.current.value;

    if (!enteredNewPassword) {
      alert(`It's required`);
      return;
    }

    if (enteredNewPassword.length < 6) {
      alert("Needs to be more than 6 characters");
      return;
    }

    props.onResetPassword(enteredNewPassword);
  };

  return (
    <form className={classes.form} onSubmit={submitHandler}>
      <div className={classes.control}>
        <label htmlFor='new-password'>New Password</label>
        <input type='password' id='new-password' ref={newPasswordInputRef} />
      </div>
      <div className={classes.action}>
        <button>Change Password</button>
      </div>
    </form>
  );
};

export default ProfileForm;
