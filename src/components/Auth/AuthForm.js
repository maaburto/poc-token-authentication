import { useEffect, useRef, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import classes from "./AuthForm.module.css";
import { Input } from "../Input";

const LoginSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Email is Required"),
  password: Yup.string().required("Password is Required"),
});

let firstRender = false;

const AuthForm = (props) => {
  firstRender = true;
  const emailInputRef = useRef();
  const [isLogin, setIsLogin] = useState(true);
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: LoginSchema,
    onSubmit: async (values) => {
      props.onLogin(values, isLogin);
    },
  });

  useEffect(() => {
    if (props.isErrorOnAuth || firstRender) {
      emailInputRef.current.applyFocus();
    }
  }, [props.isErrorOnAuth]);
  const emailIsInvalid = formik.errors.email && formik.touched.email;
  const passwordIsInValid = formik.errors.password && formik.touched.password;

  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  const classesControlFormField = (valueIsValid) =>
    `${classes.control} ${valueIsValid ? classes.invalid : ""}`;

  const emailClassesControl = classesControlFormField(emailIsInvalid);
  const passwordClassesControl = classesControlFormField(passwordIsInValid);

  return (
    <section className={classes.auth}>
      <h1>{isLogin ? "Login" : "Sign Up"}</h1>
      <form onSubmit={formik.handleSubmit}>
        <div className={emailClassesControl}>
          <label htmlFor='email'>Your Email</label>
          <Input
            type='email'
            id='email'
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            ref={emailInputRef}
          />
          {emailIsInvalid ? (
            <div className={classes["error-text"]}>{formik.errors.email}</div>
          ) : null}
        </div>
        <div className={passwordClassesControl}>
          <label htmlFor='password'>Your Password</label>
          <input
            type='password'
            id='password'
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {passwordIsInValid ? (
            <div className={classes["error-text"]}>
              {formik.errors.password}
            </div>
          ) : null}
        </div>
        <div className={classes.actions}>
          {!props.isLoading && (
            <button type='submit'>
              {isLogin ? "Login" : "Create Account"}
            </button>
          )}
          {props.isLoading && <p>Sending Request...</p>}
          <button
            type='button'
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? "Create new account" : "Login with existing account"}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
