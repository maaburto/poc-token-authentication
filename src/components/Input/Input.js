import React, { useImperativeHandle, useRef } from "react";

const Input = React.forwardRef(({ ...rest }, ref) => {
  const inputRef = useRef();

  const applyFocus = () => {
    inputRef.current.focus();
  };

  useImperativeHandle(ref, () => {
    return {
      applyFocus,
    };
  });

  return <input {...rest} ref={inputRef} /> ;
});

export default Input;
