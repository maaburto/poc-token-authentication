import { AuthenticationWidget } from "../features/AuthenticationWidget";

const AuthPage = () => {
  return <AuthenticationWidget />;
};

export default AuthPage;
